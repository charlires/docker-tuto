## Start from a Debian image with the latest version of Go installed
# and a workspace (GOPATH) configured at /go.
FROM golang:onbuild

# Copy the local package files to the container's workspace.
ADD . /go/src/docker-tuto

# Install dependencies
RUN go get -u github.com/gorilla/mux
RUN go get -u github.com/gorilla/handlers

# Compile binary
RUN go install docker-tuto

# Declare healtcheck
HEALTHCHECK --interval=5s --timeout=5s \
  CMD curl -f http://localhost:8080/healthz || exit 1

# Run program
ENTRYPOINT ["/go/bin/docker-tuto"]

# Document that the service listens on port 8080.
EXPOSE 8080